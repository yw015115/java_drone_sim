package drone;

import java.io.Serializable;
import java.util.UUID;


/**
 * The Stun Tile class implements objects that represent static obstacles that will temporarily disable adjacent drones
 * and retrieves information on their position, number of stunned drones and UUID.
 *
 * @author James Marlow
 * @version 1.1
 * @since 2020-10-28
 */
public class StunTile extends ArenaObstacle implements Serializable {
    private int stunned;

    /**
     * Constructs obstacle objects at the specified coordinates with a random UUID
     * @param x position on the x axis
     * @param y position on the y axis
     */
    public StunTile (int x, int y){
        this.x = x;
        this.y = y;
        this.stunned = 0;
        this.id = UUID.randomUUID().toString(); //construct obstacle with random unique ID
    }

    /**
     * Retrieves position of Obstacle object
     * @return string of x and y coordinates
     */
    public String getPosition(){
        return this.getX() + ", " + this.getY();
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    /**
     * Retrieves UUID of Obstacle object
     * @return string of UUID
     */
    public String getId(){
        return this.id;
    }

    /**
     * Method for setting x and y coordinates of Obstacle object
     * @param x position on the x axis
     * @param y position on the y axis
     */
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    /**
     * Calculates arena coordinates of adjacent locations and checks if any are occupied by drones.
     * If a drone is adjacent it is temporarily stunned.
     * @param arena
     */
    void checkAdjaceny(DroneArena arena){
        int[][] adjacentLocs = new int[8][2];
        adjacentLocs[0][0] = this.getX()-1; adjacentLocs[0][1] = this.getY()+1;
        adjacentLocs[1][0] = this.getX()-1; adjacentLocs[1][1] = this.getY();
        adjacentLocs[2][0] = this.getX()-1; adjacentLocs[2][1] = this.getY()-1;
        adjacentLocs[3][0] = this.getX(); adjacentLocs[3][1] = this.getY()+1;
        adjacentLocs[4][0] = this.getX(); adjacentLocs[4][1] = this.getY()-1;
        adjacentLocs[5][0] = this.getX()+1; adjacentLocs[5][1] = this.getY()+1;
        adjacentLocs[6][0] = this.getX()+1; adjacentLocs[6][1] = this.getY();
        adjacentLocs[7][0] = this.getX()+1; adjacentLocs[7][1] = this.getY()-1;

        for(BasicDrone drone : arena.drones){
            for (int row = 0; row < adjacentLocs.length; row++){
                if((adjacentLocs[row][0] == drone.getX() & adjacentLocs[row][1] == drone.getY()) &&
                        !drone.getDisabledStatus()){
                    this.stunned += 1;
                    drone.setDisabledStatus(true);
                }
                else if((adjacentLocs[row][0] == drone.getX() & adjacentLocs[row][1] == drone.getY()) &&
                        drone.getDisabledStatus()){
                    if(!(drone instanceof HunterDrone) && drone.timeDisabled == 2){
                        drone.setDisabledStatus(false);
                    }
                    else if(drone.timeDisabled == 1){
                        drone.setDisabledStatus(false);
                    }
                }
            }
        }
    }

    /**
     * Retrieves information on Obstacle UUID and coordinates
     * @return string containing UUID and coordinates
     */
    public String toString(){
        return "Stun Tile " + this.getId() + " is at X, Y " + this.getPosition() + " stunned drones: " + this.stunned + "\n";
    }

    /**
     * Checks if Obstacle object is present at specified location
     * @param sx x axis coordinate to check against
     * @param sy y axis coordinate to check against
     * @return true if Obstacle present, false if obstacle absent
     */
    public boolean isHere(int sx, int sy){
        return this.x == sx && this.y == sy;
    }

    /**
     * Places indicator of the Obstacle within the console canvas to match it's current position.
     * @param c ConsoleCanvas object that will present the Obstacle's position.
     */
    public void displayObstacle(ConsoleCanvas c){
        c.canvas[this.y+1][this.x+1] = 'S';
    }

    public static void main(String[] args){
        InternalWall one = new InternalWall(5,3);
        InternalWall two = new InternalWall(7,9);
        System.out.println(one.toString());
        System.out.println(two.toString());
    }
}
