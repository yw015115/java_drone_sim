package drone;

/**
 * Abstract sub class for arena drones that provides a display method and methods for controlling movement,
 * all other required methods are provided in the Super class Arena Object.
 * @author James Marlow
 * @version 1.0
 * @since 2020-10-28
 */

public abstract class BasicDrone extends ArenaObject{
    Direction direction;
    boolean disabled; //flag to indicate if drone can move, true = disabled, false = can move
    int timeDisabled; //track time unable to move

    public abstract Direction getDirection();

    public abstract void setDirection(Direction direction);

    /**
     * Method to access and return attribute that indicates if the drone can move.
     * @return true if disabled, otherwise return false
     */
    public abstract boolean getDisabledStatus();

    /**
     * Method for setting if the drone can move or is disabled.
     * @param status set to true to disable drone movement or set to false to (re) enable movement.
     */
    public abstract void setDisabledStatus(boolean status);

    public abstract void tryToMove(DroneArena arena);

    /**
     * Method for predicting where a drone may move too and providing a mechanism for collision avoidance.
     * @return array of X and Y coordinates.
     */
    public abstract int[] predictNewPosition();

    /**
     * Places indicator of the drone within the console canvas to match it's current position.
     * @param c ConsoleCanvas object that will present the drone's position.
     */
    public abstract void displayDrone(ConsoleCanvas c);
}
