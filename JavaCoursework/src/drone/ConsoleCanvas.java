package drone;

import java.util.Arrays;

/**
 * The ConsoleCanvas class implements objects that represent a DroneArena object of the appropriate
 * size with a boundary wall and indicators marking the positions of individual drones within the arena
 * which is then displayed within the console.
 *
 * @author James Marlow
 * @version 1.0
 * @since 2020-10-19
 */
public class ConsoleCanvas {
    char[][] canvas;
    DroneArena arena;

    /**
     * Constructs ConsoleCanvas objects to represent provided DroneArena.
     * @param x Length of cArena
     * @param y Height of cArena
     * @param cArena DroneArena object to represent
     */
    public ConsoleCanvas(int x, int y, DroneArena cArena){
        this.arena = cArena;

        //Create, set size and boundaries of 2d canvas grid
        this.canvas = new char[y+2][x+2];
        Arrays.fill(this.canvas[0], '#'); //Create top
        Arrays.fill(this.canvas[this.canvas.length-1], '#'); //Create bottom

        //Create sides and fill centre
        for(int i = 1; i < this.canvas.length-1; i++){
            for(int j = 0; j < this.canvas[i].length; j++){
                if(j == 0 || j == this.canvas[i].length-1){
                    this.canvas[i][j] = '#';
                }
                else{
                    this.canvas[i][j] = ' ';
                }
            }
        }
    }

    /**
     * Inserts a drone into the canvas at the given location and displays an indicator at
     * every location a drone is present.
     * @param x position along length of droneArena object
     * @param y position along height of droneArena object
     * @param d Direction enum value showing direction of drone travel
     * @param c Character to use as indicator of drone presence at a location
     */
    public void showIt(int x, int y, Direction d, int t, char c){
        arena.addDrone(x, y, d, t);
        for(int i = 1; i < this.canvas.length-1; i++){
            for(int j = 1; j < this.canvas[i].length-1; j++){
                if(arena.getDroneAt(j-1,i-1)){
                    this.canvas[i][j] = c;
                }
            }
        }
    }
    public void setSize(int x, int y){
        this.canvas = new char[y+2][x+2];
    }

    /**
     * Method for drawing canvas.
     * @return string representation of canvas, including border walls and drone locations.
     */
    public String toString(){
        StringBuilder canvasDisplay = new StringBuilder();
        for (int i = 0; i < this.canvas.length; i++){
            canvasDisplay.append(this.canvas[i]);
            canvasDisplay.append('\n');
        }
        return this.arena.toString() + canvasDisplay.toString() + "\n";
    }

    public static void main(String[] args){
        DroneArena d = new DroneArena(10, 5);
        d.addDrone(1);
        ConsoleCanvas c = new ConsoleCanvas(12, 7, d);
        c.arena.showDrones(c);
        System.out.println(c.toString());
    }
}
