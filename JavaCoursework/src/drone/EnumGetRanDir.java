package drone;

import java.util.Random;

/**
 * Provides a class and enum for specifying the direction a drone is travelling.
 *
 * @author James Marlow
 * @version 1.1
 * @since 2020-10-28
 */
public class EnumGetRanDir{
    public static void main(String[] args){
        System.out.println(Direction.getRandomDirection());
    }
}

enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST,
    NORTH_EAST,
    NORTH_WEST,
    SOUTH_EAST,
    SOUTH_WEST;

    /**
     * @return Random Direction enum value.
     */
    public static Direction getRandomDirection(){
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }

    /**
     * Produces a random number from within a range that excludes specific integers.
     * @param start number to begin range.
     * @param end number to end range.
     * @param exclude numbers to exclude from range.
     * @return random number
     */
    public static int getRandomInt(int start, int end, int... exclude){
        Random random = new Random();
        int randomInt = start + random.nextInt(end - start + 1 - exclude.length);
        for (int ex : exclude) {
            if (randomInt < ex) {
                break;
            }
            randomInt++;
        }
        return randomInt;
    }
}
