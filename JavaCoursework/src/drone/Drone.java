package drone;

import java.io.Serializable;
import java.util.UUID;

/**
 * The Drone class implements objects that represent drones
 * and controls how they move in 2d space and retrieves information
 * on their position and UUID.
 *
 * @author James Marlow
 * @version 1.1
 * @since 2020-10-28
 */
public class Drone extends BasicDrone implements Serializable {
    /**
     * Constructs Drone objects at the specified coordinates with a random UUID
     * @param x position on the x axis
     * @param y position on the y axis
     */
   public Drone(int x, int y, Direction d){
       //Setting current position and direction of travel allows for tracking and drawing the drone's movement
       this.x = x;
       this.y = y;
       this.direction = d;
       this.disabled = false;
       this.timeDisabled = 0;
       this.id = UUID.randomUUID().toString(); //construct drone with random unique ID
   }

    /**
     * Retrieves position of Drone object
     * @return string of x and y coordinates
     */
    public String getPosition(){
        return this.getX() + ", " + this.getY();
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean getDisabledStatus(){
        return this.disabled;
    }

    public void  setDisabledStatus(boolean status){
        this.disabled = status;
    }

    /**
     * Retrieves UUID of Drone object
     * @return string of UUID
     */
    public String getId(){
        return this.id;
    }

    /**
     * Method for setting x and y coordinates of Drone object
     * @param x position on the x axis
     * @param y position on the y axis
     */
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    /**
     * Retrieves information on Drone UUID and coordinates
     * @return string containing UUID and coordinates
     */
    public String toString(){
       return "Drone " + this.getId() + " is at X, Y " + this.getPosition() + " and moving " + this.direction + "\n";
    }

    /**
     * Checks if drone object is present at specified location
     * @param sx x axis coordinate to check against
     * @param sy y axis coordinate to check against
     * @return true if drone present, false if drone absent
     */
    public boolean isHere(int sx, int sy){
        return this.x == sx && this.y == sy;
    }

    /**
     * Moves drones around an arena in a specified distance, drones will move according to their direction attribute
     * and will turn if the distance would move them out of the arena bounds or collide with another drone.
     * @param arena DroneArena object that the drones are contained in.
     */
    public void tryToMove(DroneArena arena){
        if(this.disabled && this.timeDisabled < 2){
            timeDisabled += 1;
            return;
        }
        int[] predict = predictNewPosition();
        int changeX = predict[0], changeY = predict[1], dirIndex = predict[2];
        //Check if projected space available
        if (arena.canMoveHere(changeX, changeY)) {
            this.x = changeX;
            this.y = changeY;
        }
        //When blocked turn in random direction
        else {
            while(!arena.canMoveHere(changeX, changeY)){
                this.direction = Direction.values()[Direction.getRandomInt(0, 7, dirIndex)];
                predict = predictNewPosition();
                changeX = predict[0]; changeY = predict[1]; dirIndex = predict[2];
            }

            this.x = changeX;
            this.y = changeY;
            System.out.println("Drone " + this.id + " Turning " + this.direction + "\n");
        }
    }

    public int[] predictNewPosition(){
        int changeX = this.x;
        int changeY = this.y;
        int dirIndex = 0;
        switch (this.direction) {
            case NORTH -> changeY -= 1;
            case EAST -> {
                changeX += 1;
                dirIndex = 1;
            }
            case SOUTH -> {
                changeY += 1;
                dirIndex = 2;
            }
            case WEST -> {
                changeX -= 1;
                dirIndex = 3;
            }
            case NORTH_EAST -> {
                changeY -= 1;
                changeX += 1;
                dirIndex = 4;
            }
            case NORTH_WEST -> {
                changeY -= 1;
                changeX -= 1;
                dirIndex = 5;
            }
            case SOUTH_EAST -> {
                changeY += 1;
                changeX += 1;
                dirIndex = 6;
            }
            case SOUTH_WEST -> {
                changeY += 1;
                changeX -= 1;
                dirIndex = 7;
            }
        }
        return new int[]{changeX, changeY, dirIndex};
    }

    /**
     * Places indicator of the drone within the console canvas to match it's current position.
     * @param c ConsoleCanvas object that will present the drone's position.
     */
    public void displayDrone(ConsoleCanvas c){
        c.canvas[this.y+1][this.x+1] = 'D';
    }

    public static void main(String[] args){
        Drone one = new Drone(5,3, Direction.getRandomDirection());
        Drone two = new Drone(7,9, Direction.getRandomDirection());
        System.out.println(one.toString());
        System.out.println(two.toString());
    }
}
