package drone;

import java.io.Serializable;
import java.util.UUID;

/**
 * The Drone class implements objects that represent drones
 * and controls how they move in 2d space and retrieves information
 * on their position, number of captured drones and UUID.
 *
 * @author James Marlow
 * @version 1.0
 * @since 2020-12-21
 */
public class HunterDrone extends BasicDrone implements Serializable {
    private int captured;

    /**
     * Constructs Drone objects at the specified coordinates with a random UUID
     * @param x position on the x axis
     * @param y position on the y axis
     */
   public HunterDrone(int x, int y, Direction d){
       //Setting current position and direction of travel allows for tracking and drawing the drone's movement
       this.x = x;
       this.y = y;
       this.direction = d;
       this.disabled = false;
       this.timeDisabled = 0;
       this.id = UUID.randomUUID().toString(); //construct drone with random unique ID
       this.captured = 0;
   }

    /**
     * Retrieves position of Drone object
     * @return string of x and y coordinates
     */
    public String getPosition(){
        return this.getX() + ", " + this.getY();
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public Direction getDirection(){
        return this.direction;
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public boolean getDisabledStatus(){
        return this.disabled;
    }

    public void  setDisabledStatus(boolean status){
        this.disabled = status;
    }

    /**
     * Retrieves UUID of Drone object
     * @return string of UUID
     */
    public String getId(){
        return this.id;
    }

    /**
     * Method for setting x and y coordinates of Drone object
     * @param x position on the x axis
     * @param y position on the y axis
     */
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    /**
     * Retrieves information on Drone UUID and coordinates
     * @return string containing UUID and coordinates
     */
    public String toString(){
       return "Hunter Drone " + this.getId() + " is at X, Y " + this.getPosition() + " and moving " + this.direction +
               "Captured " + this.captured + " drones\n";
    }

    /**
     * Checks if drone object is present at specified location
     * @param sx x axis coordinate to check against
     * @param sy y axis coordinate to check against
     * @return true if drone present, false if drone absent
     */
    public boolean isHere(int sx, int sy){
        return this.x == sx && this.y == sy;
    }

    /**
     * Checks all non hunter drone locations and sets direction of movement to intercede closest drone
     * @param arena object containing drones and their locations
     */
    private void seekClosetDrone(DroneArena arena){
        int closestX = this.getX();
        int closestY = this.getY();
        int distX, distY, shortestX = 0, shortestY = 0;
        for(BasicDrone drone : arena.drones){
            if(drone instanceof HunterDrone){
                continue;
            }
            distX = convertNegative(this.getX() - drone.getX());
            distY = convertNegative(this.getY() - drone.getY());
            if(distX < closestX){
                closestX = drone.getX();
                shortestX = distX;
            }
            if(distY < closestY){
                closestY = drone.getY();
                shortestY = distY;
            }
        }
        if(shortestX < shortestY){
            if(closestX > this.getX()){
                this.direction = Direction.EAST;
            }
            else{
                this.direction = Direction.WEST;
            }
        }
        else if(shortestY > this.getY()){
            if(closestY > this.getY()){
                this.direction = Direction.SOUTH;
            }
            else{
                this.direction = Direction.NORTH;
            }
        }
    }

    /**
     * Calculates arena coordinates of adjacent locations and checks if any are occupied by non hunter drones.
     * If a non hunter drone is adjacent it is removed from the arena permanently.
     * @param arena
     */
    private void attemptCaptureDrone(DroneArena arena){
        int[][] adjacentLocs = new int[8][2];
        adjacentLocs[0][0] = this.getX()-1; adjacentLocs[0][1] = this.getY()+1;
        adjacentLocs[1][0] = this.getX()-1; adjacentLocs[1][1] = this.getY();
        adjacentLocs[2][0] = this.getX()-1; adjacentLocs[2][1] = this.getY()-1;
        adjacentLocs[3][0] = this.getX(); adjacentLocs[3][1] = this.getY()+1;
        adjacentLocs[4][0] = this.getX(); adjacentLocs[4][1] = this.getY()-1;
        adjacentLocs[5][0] = this.getX()+1; adjacentLocs[5][1] = this.getY()+1;
        adjacentLocs[6][0] = this.getX()+1; adjacentLocs[6][1] = this.getY();
        adjacentLocs[7][0] = this.getX()+1; adjacentLocs[7][1] = this.getY()-1;

        for(int i = 0; i > arena.drones.size(); i++){
            BasicDrone drone = arena.drones.get(i);
            if(!(drone instanceof HunterDrone)){
                for (int row = 0; row < adjacentLocs.length; row++){
                    if(adjacentLocs[row][0] == drone.getX() & adjacentLocs[row][1] == drone.getY()){
                        this.captured += 1;
                        arena.drones.remove(drone);
                    }
                }
            }
        }
    }

    /**
     * Converts negative integers to positives for purpose of calculating distances between two drones.
     * @param i integer to positively sign.
     * @return positive signed i.
     */
    private int convertNegative(int i){
        if(i >= 0){
            return i;
        }
        else{
            return i + i * 2;
        }
    }

    /**
     * Moves drones around an arena in a specified distance, drones will move according to their direction attribute
     * and will turn if the distance would move them out of the arena bounds or collide with another drone.
     * @param arena DroneArena object that the drones are contained in.
     */
    public void tryToMove(DroneArena arena){
        if(this.disabled && this.timeDisabled < 1){
            timeDisabled += 1;
            return;
        }
        seekClosetDrone(arena);
        int[] predict = predictNewPosition();
        int changeX = predict[0], changeY = predict[1], dirIndex = predict[2];
        //Check if projected space available
        if (arena.canMoveHere(changeX, changeY)) {
            this.x = changeX;
            this.y = changeY;
        }
        //When blocked turn in random direction
        else {
            while(!arena.canMoveHere(changeX, changeY)){
                this.direction = Direction.values()[Direction.getRandomInt(0, 7, dirIndex)];
                predict = predictNewPosition();
                changeX = predict[0]; changeY = predict[1]; dirIndex = predict[2];
            }

            this.x = changeX;
            this.y = changeY;
            System.out.println("Drone " + this.id + " Turning " + this.direction + "\n");
        }
        attemptCaptureDrone(arena);
    }

    public int[] predictNewPosition(){
        int changeX = this.x;
        int changeY = this.y;
        int dirIndex = 0;
        switch (this.direction) {
            case NORTH -> changeY -= 1;
            case EAST -> {
                changeX += 1;
                dirIndex = 1;
            }
            case SOUTH -> {
                changeY += 1;
                dirIndex = 2;
            }
            case WEST -> {
                changeX -= 1;
                dirIndex = 3;
            }
            case NORTH_EAST -> {
                changeY -= 1;
                changeX += 1;
                dirIndex = 4;
            }
            case NORTH_WEST -> {
                changeY -= 1;
                changeX -= 1;
                dirIndex = 5;
            }
            case SOUTH_EAST -> {
                changeY += 1;
                changeX += 1;
                dirIndex = 6;
            }
            case SOUTH_WEST -> {
                changeY += 1;
                changeX -= 1;
                dirIndex = 7;
            }
        }
        return new int[]{changeX, changeY, dirIndex};
    }

    /**
     * Places indicator of the drone within the console canvas to match it's current position.
     * @param c ConsoleCanvas object that will present the drone's position.
     */
    public void displayDrone(ConsoleCanvas c){
        c.canvas[this.y+1][this.x+1] = 'H';
    }

    public static void main(String[] args){
        HunterDrone one = new HunterDrone(5,3, Direction.getRandomDirection());
        HunterDrone two = new HunterDrone(7,9, Direction.getRandomDirection());
        System.out.println(one.toString());
        System.out.println(two.toString());
    }
}
