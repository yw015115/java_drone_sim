package drone;

/**
 * Abstract sub class for arena obstacles that provides a display method, all other required methods are provided
 * in the Super class Arena Object.
 * @author James Marlow
 * @version 1.0
 * @since 2020-10-28
 */

public abstract class ArenaObstacle extends ArenaObject{
    public abstract void displayObstacle(ConsoleCanvas c);
}
