package drone;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * The Drone Arena class implements arena objects that stores drone objects
 * and allows controls for the size of the arena, the number of drones and where
 * they are placed within the arena.
 *
 * @author James Marlow
 * @version 1.1
 * @since 2020-10-28
 */
public class DroneArena implements Serializable{
    private int length, height;
    ArrayList<BasicDrone> drones;
    ArrayList<ArenaObstacle> obstacles;
    ArrayList<int[]> occupied_locs;
    private Random gen;

    /**
     * DroneArena class constructor
     * Creates objects with set size parameters
     * @param x length of arena object
     * @param y height of arena object
     */
    public DroneArena(int x, int y){
        this.setSize(x, y);
        this.drones = new ArrayList<>(); //Allows arena object and store and access variable numbers of drone objects.
        this.obstacles = new ArrayList<>();
        this.occupied_locs = new ArrayList<>();
        this.gen = new Random(); //Random number generator for adding drones to random locations.
    }

    //Getters and setters provide the primary programming interface for arena configuration.
    public String getSize(){
        return "Length: " + this.getLength() + ", Height: " + this.getHeight();
    }

    public int getLength(){
        return this.length;
    }

    public int getHeight(){
        return this.height;
    }

    /**
     * Method for setting size of arena object
     * @param x length of arena object
     * @param y height of arena object
     */
    public void setSize(int x, int y){
        setLength(x);
        setHeight(y);
    }

    private void setLength(int x){
        this.length = x;
    }

    private void setHeight(int y){
        this.height = y;
    }

    /**
     * Wrapper method for providing single interface for adding both drone and obstacle objects to arena.
     * @param i switch case identifier
     */
    public void addObject(int i){
        switch (i){
            case 1 -> addDrone(3);
            case 2 -> addDrone(2);
            case 3 -> addDrone(1);
            case 4 -> addObstacle(1);
            case 5 -> addObstacle(2);
        }
    }

    /**
     * Method for adding drone object to random location within arena object
     */
    public void addDrone(int i){
        int x, y;
        do{
            x = gen.nextInt(this.length);
            y = gen.nextInt(this.height);
        }
        while(this.getDroneAt(x, y));
        switch (i){
            case 1 -> this.drones.add(new BorderDrone(x, y, Direction.getRandomDirection()));
            case 2 -> this.drones.add(new HunterDrone(x, y, Direction.getRandomDirection()));
            case 3 -> this.drones.add(new Drone(x, y, Direction.getRandomDirection()));
        }
    }

    /**
     * Method for adding obstacle object to random location within arena object
     */
    public void addObstacle(int i){
        int x, y;
        do{
            x = gen.nextInt(this.length);
            y = gen.nextInt(this.height);
        }
        while(this.getDroneAt(x, y));
        switch (i){
           case 1 -> this.obstacles.add(new InternalWall(x, y));
           case 2 -> this.obstacles.add(new StunTile(x, y));
        }
    }

    /**
     * Overloaded method for adding drone object to specified location within arena object
     * @param x position on x axis/length
     * @param y position on y axis/height
     */
    public void addDrone(int x, int y, Direction d, int t){
        if(this.getDroneAt(x, y)){
            System.out.println("SPACE IS ALREADY OCCUPIED");
            return;
        }
        switch(t) {
            case 1 -> this.drones.add(new BorderDrone(x, y, Direction.getRandomDirection()));
            case 2 -> this.drones.add(new HunterDrone(x, y, Direction.getRandomDirection()));
            case 3 -> this.drones.add(new Drone(x, y, Direction.getRandomDirection()));
        }
    }

    /**
     * Overloaded method for adding obstacle object to specified location within arena object
     * @param x position on x axis/length
     * @param y position on y axis/height
     */
    public void addObstacle(int x, int y, int t){
        if(this.getDroneAt(x, y)){
            System.out.println("SPACE IS ALREADY OCCUPIED");
            return;
        }
        switch(t){
            case 1 -> this.obstacles.add(new InternalWall(x, y));
            case 2 -> this.obstacles.add(new StunTile(x, y));
        }
    }

    /**
     * Method for retrieving size of arena and information on each drone
     * @return string containing arena size, drone UUID and position
     */
    public String toString(){
        StringBuilder objectLocs = new StringBuilder();
        for (BasicDrone drone : drones){
            objectLocs.append(drone.toString());
        }
        for (ArenaObstacle obstacle : obstacles){
            objectLocs.append(obstacle.toString());
        }
        return "Arena size is " + this.getSize() + "\n" + objectLocs;
    }

    /**
     * Checks if a drone is at specified location within arena
     * @param x position on x axis/length
     * @param y position on y axis/height
     * @return true if location at x y is occupied, false if empty
     */
    public boolean getDroneAt(int x, int y){
        for (BasicDrone drone : drones) {
            if (drone.isHere(x, y)) {
                return true;
            }
        }
        return false;
    }

    public boolean getObstacleAt(int x, int y){
        for (ArenaObstacle obstacle : obstacles) {
            if (obstacle.isHere(x, y)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method for checking if any drones are within the adjacency of any stun tiles within arena.
     */
    public void checkStunTiles(){
        for(ArenaObstacle obstacle : obstacles){
            if(obstacle instanceof StunTile){
                ((StunTile) obstacle).checkAdjaceny(this);
            }
        }
    }

    /**
     * Method for resetting arena drone disabled status.
     */
    public void checkStunnedDrones(){
        for(BasicDrone drone : drones){
            if(drone.getDisabledStatus()){
                drone.setDisabledStatus(false);
            }
        }
    }

    /**
     * Checks if a drone can move to a specified location within arena
     * @param x position on x axis/length
     * @param y position on y axis/height
     * @return true if the location is within arena boundaries and doesn't contain another drone
     */
    public boolean canMoveHere(int x, int y){
        //Check if any obstacles block the drone
        for (ArenaObstacle obstacle : this.obstacles){
            if(x == obstacle.getX() & y == obstacle.getY()){
                return false;
            }
        }
        //Check if the drone is moving within arena bounds
        return (x <= this.length-1 && x > 0) && (y <= this.height-1 && y > 0) &&
                (!this.getDroneAt(x, y) || !this.getObstacleAt(x, y));
    }

    /**
     * Attempts to move all drones within the arena
     * @param arena DroneArena object that contains the drones, allows the arena to pass itself
     * when the method is called from another class object
     */
    public void moveAllDrones(DroneArena arena){
        for(int i = 0; i < arena.drones.size(); i++){
            arena.drones.get(i).tryToMove(arena);
        }
    }

    /**
     * Displays indicators for all drones contained in the arena
     * @param c ConsoleCanvas object that the drones will be presented on
     */
    public void showDrones(ConsoleCanvas c){
        for (BasicDrone drone : drones){
            drone.displayDrone(c);
        }
    }

    /**
     * Displays indicators for all obstacles contained in the arena
     * @param c ConsoleCanvas object that the drones will be presented on
     */
    public void showObstacles(ConsoleCanvas c) {
        for (ArenaObstacle obstacle : obstacles){
            obstacle.displayObstacle(c);
        }
    }

    public static void main(String[] args){
        DroneArena one = new DroneArena(20,10);
        one.addDrone(1);
        one.addDrone(10, 10, Direction.EAST, 3);
        one.addObstacle(1);
        System.out.println(one.getDroneAt(10, 10));
        System.out.println(one.toString());
    }
}
