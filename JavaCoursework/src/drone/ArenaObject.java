package drone;

/**
 * Abstract class for defining basic methods required by all objects placed within an arena.
 * @author James Marlow
 * @version 1.0
 * @since 2020-10-28
 */

public abstract class ArenaObject {
    String id;
    int x, y;

    abstract String getPosition();

    abstract int getX();

    abstract int getY();

    /**
     * Retrieves UUID of Obstacle object
     * @return string of UUID
     */
    public abstract String getId();

    /**
     * Method for setting x and y coordinates of Obstacle object
     * @param x position on the x axis
     * @param y position on the y axis
     */
    public abstract void setPosition(int x, int y);

    public abstract void setX(int x);

    public abstract void setY(int y);

    /**
     * Retrieves information on Obstacle UUID and coordinates
     * @return string containing UUID and coordinates
     */
    public abstract String toString();

    /**
     * Checks if Obstacle object is present at specified location
     * @param sx x axis coordinate to check against
     * @param sy y axis coordinate to check against
     * @return true if obstacle present, false if obstacle absent
     */
    public abstract boolean isHere(int sx, int sy);
}
