package drone;

import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * The DroneInterface class provides a text based interface within the console in order to manage the arena and it's
 * associated drones and console canvas.
 *
 * @author James Marlow
 * @version 1.1
 * @since 2020-10-27
 */

public class DroneInterface{
        private DroneArena myArena;
        private ConsoleCanvas myCanvas;

    /**
     * Handles keyboard input and maps keys to specific program functionalities.
     */
    public DroneInterface(){
        final Scanner s;
        File dir = new File(".\\saves");

        File default_save = new File(".\\saves\\default.ser");

        //Create arena saves directory on first time run
        if(!dir.exists()){
            dir.mkdir();
        }

        //If saves directory is empty setup default arena config, otherwise load default arena save file
        if(dir.length() == 0){
            this.myArena = new DroneArena(20, 10);
            this.myArena.addDrone(1);
            this.myArena.addDrone(1);
            this.myArena.addDrone(2);
            this.myArena.addObstacle(1);
            this.myArena.addObstacle(2);
        }
        else if (default_save.exists()){
            try {
                FileInputStream fInStream = new FileInputStream(default_save);
                ObjectInputStream oInStream = new ObjectInputStream(fInStream);
                this.myArena = (DroneArena) oInStream.readObject();
            }
            catch (IOException | ClassNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }

        s = new Scanner(System.in); //Begin capturing keyboard input
            myArena = new DroneArena(20, 6);
            char ch;

            //Until exit character is given provide interface menu options
            do {
                System.out.print("Enter (A)dd drone or obstacle, get (I)nformation, (D)isplay drones and obstacles, " +
                        "(M)ove drones, (N)ew arena, (R)esize arena, (S)ave arena, (L)oad arena or e(X)it > ");
                ch = s.next().charAt(0);
                s.nextLine();
                switch (ch) {
                    case 't', 'T' -> {
                        myArena.addDrone(10, 3, Direction.SOUTH, 3);
                        myArena.addDrone(13, 5, Direction.SOUTH, 3);
                        myArena.addDrone(10, 4, Direction.SOUTH, 2);
                        myArena.addDrone(18, 5, Direction.SOUTH, 1);
                        myArena.addObstacle(4, 4, 1);
                        myArena.addObstacle(10, 4, 2);
                        myArena.addObstacle(13, 2, 2);
                    }
                    case 'd', 'D' -> this.doDisplay(); //Display arena and drone information
                    //Add drone or obstacle objects to arena
                    case 'A', 'a' -> {
                        try {
                            System.out.println("Enter an integer to select object type to add:\n" +
                                    "1: add a Drone\n2: add a Hunter Drone\n3: add a Border Drone" +
                                    "\n4: add an Internal Wall\n5: add a Stun Tile");
                            myArena.addObject(s.nextInt()); //Add drone or obstacle to random location within arena
                        }
                        catch(Exception e) {
                            System.err.println("Invalid input, please enter an integer.");
                        }
                    }
                    case 'I', 'i' -> System.out.print(myArena.toString()); //Display arena information
                    //Move all drones a given number of arena/canvas spaces
                    case 'M', 'm' -> {
                        System.out.println("Enter an integer for the distance to move:");
                        int distance = 0;
                        try {
                            distance = s.nextInt();
                        }
                        catch(Exception e) {
                            System.err.println("Invalid input, please enter an integer.");
                        }

                        if(!myArena.drones.isEmpty() && distance != 0){
                            int i = 1;
                            while(i <= distance){
                                myArena.moveAllDrones(myArena); //Update drone locations
                                this.myArena.checkStunTiles();
                                doDisplay(); //Refresh display canvas
                                try{
                                    //delay switch between canvas refreshes to create animation
                                    TimeUnit.MILLISECONDS.sleep(250);
                                }
                                catch (InterruptedException exception){
                                    System.err.format("Exception: %s%n", exception);
                                }
                                i++;
                            }
                        }
                        else {
                            System.err.println("Arena is empty!");
                        }
                    }

                    //Create new empty arena with configuration options
                    case 'N', 'n' -> {
                        try {
                            System.out.println("Entering new arena dimensions:\n Please enter X integer value.\n");
                            int x = s.nextInt();
                            System.out.println("Please enter Y integer value.\n");
                            int y = s.nextInt();
                            this.myArena = new DroneArena(x, y);
                        }
                        catch (Exception e) {
                            System.err.println("Invalid input, please enter an integer.\n");
                        }
                    }

                    //Resize current arena
                    case 'R', 'r' -> {
                        try {
                            System.out.println("Entering new arena dimensions:\n Please enter X integer value.\n");
                            int x = s.nextInt();
                            System.out.println("Please enter Y integer value.\n");
                            int y = s.nextInt();
                            this.myArena.setSize(x, y);

                            for (BasicDrone drone : this.myArena.drones){
                                int adjust_drone_x = x, adjust_drone_y = y;
                                while(drone.getX() > this.myArena.getLength()){
                                    adjust_drone_x--;
                                    drone.setX(adjust_drone_x);
                                }
                                while(drone.getY() > this.myArena.getHeight()){
                                    adjust_drone_y--;
                                    drone.setY(adjust_drone_y);
                                }
                            }
                            for (ArenaObstacle obstacle: this.myArena.obstacles){
                                int adjust_obstacle_x = x, adjust_obstacle_y = y;
                                while(obstacle.getX() > this.myArena.getLength()){
                                    adjust_obstacle_x--;
                                    obstacle.setX(adjust_obstacle_x);
                                }
                                while(obstacle.getY() > this.myArena.getHeight()){
                                    adjust_obstacle_y--;
                                    obstacle.setY(adjust_obstacle_y);
                                }
                            }
                        }
                        catch (Exception e){
                            System.err.println("Invalid input, please enter an integer.\n");
                        }
                    }

                    //Save current arena with drone states
                    case 'S', 's' -> {
                        int response;
                        File saveFile;

                        //Creates file chooser at the package saves dir with file filters
                        JFileChooser fileChooser = new JFileChooser(".\\saves");
                        FileNameExtensionFilter filter = new FileNameExtensionFilter(".ser", "ser");
                        fileChooser.setFileFilter(filter);
                        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

                        //Opens and scan contents of UI to save to disk
                        response = fileChooser.showSaveDialog(null);
                        FileOutputStream fOutStream;
                        ObjectOutputStream oOutStream;
                        if(response == JFileChooser.APPROVE_OPTION){
                            saveFile = fileChooser.getSelectedFile();
                            try {
                                fOutStream = new FileOutputStream(saveFile);
                                oOutStream = new ObjectOutputStream(fOutStream);
                                oOutStream.writeObject(this.myArena);
                            }
                            catch (IOException e) {
                                System.out.println("An error occurred.");
                                e.printStackTrace();
                            }

                        }
                    }

                    //Select and load saved arena with configuration and drone states
                    case 'L', 'l' -> {
                        int response;
                        File loadFile;

                        //Creates file chooser at the package saves dir with file filters
                        JFileChooser fileChooser = new JFileChooser(".\\saves");
                        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                        FileNameExtensionFilter filter = new FileNameExtensionFilter(".ser", "ser");
                        fileChooser.setFileFilter(filter);

                        //Opens and scan contents of UI to load from disk
                        response = fileChooser.showOpenDialog(null);
                        if(response == JFileChooser.APPROVE_OPTION){
                            loadFile = fileChooser.getSelectedFile();
                            if(loadFile.isFile()){
                                try {
                                    FileInputStream fInStream = new FileInputStream(loadFile);
                                    ObjectInputStream oInStream = new ObjectInputStream(fInStream);
                                    this.myArena = (DroneArena) oInStream.readObject();
                                }
                                catch (IOException | ClassNotFoundException e) {
                                    System.out.println("An error occurred.");
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    case 'x' -> ch = 'X';
                }
            } while (ch != 'X');

            s.close();
        }

    /**
     * Draws ConsoleCanvas object to size appropriate for the arena and marks drone locations before displaying
     * the canvas to the console.
     */
    private void doDisplay(){
        int x = myArena.getLength();
        int y = myArena.getHeight();
        this.myCanvas = new ConsoleCanvas(x, y, this.myArena);
        this.myArena.showDrones(this.myCanvas);
        this.myArena.showObstacles(this.myCanvas);

        System.out.print(this.myCanvas.toString());
    }

    public static void main(String[] args){
        DroneInterface r = new DroneInterface();
    }
}
