package drone;

import java.io.Serializable;
import java.util.UUID;

/**
 * The Internal Wall class implements objects that represent static obstacles
 * and retrieves information on their position and UUID.
 *
 * @author James Marlow
 * @version 1.1
 * @since 2020-10-28
 */
public class InternalWall extends ArenaObstacle implements Serializable {
    /**
     * Constructs obstacle objects at the specified coordinates with a random UUID
     * @param x position on the x axis
     * @param y position on the y axis
     */
    public InternalWall(int x, int y){
        this.x = x;
        this.y = y;
        this.id = UUID.randomUUID().toString(); //construct obstacle with random unique ID
    }

    /**
     * Retrieves position of Obstacle object
     * @return string of x and y coordinates
     */
    public String getPosition(){
        return this.getX() + ", " + this.getY();
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    /**
     * Retrieves UUID of Obstacle object
     * @return string of UUID
     */
    public String getId(){
        return this.id;
    }

    /**
     * Method for setting x and y coordinates of Obstacle object
     * @param x position on the x axis
     * @param y position on the y axis
     */
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    /**
     * Retrieves information on Obstacle UUID and coordinates
     * @return string containing UUID and coordinates
     */
    public String toString(){
        return "Internal Wall " + this.getId() + " is at X, Y " + this.getPosition() + "\n";
    }

    /**
     * Checks if Obstacle object is present at specified location
     * @param sx x axis coordinate to check against
     * @param sy y axis coordinate to check against
     * @return true if Obstacle present, false if obstacle absent
     */
    public boolean isHere(int sx, int sy){
        return this.x == sx && this.y == sy;
    }

    /**
     * Places indicator of the Obstacle within the console canvas to match it's current position.
     * @param c ConsoleCanvas object that will present the Obstacle's position.
     */
    public void displayObstacle(ConsoleCanvas c){
        c.canvas[this.y+1][this.x+1] = '#';
    }

    public static void main(String[] args){
        InternalWall one = new InternalWall(5,3);
        InternalWall two = new InternalWall(7,9);
        System.out.println(one.toString());
        System.out.println(two.toString());
    }
}
