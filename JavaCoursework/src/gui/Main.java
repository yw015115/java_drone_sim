package gui;

import drone.ArenaObstacle;
import drone.BasicDrone;
import drone.ConsoleCanvas;
import drone.DroneArena;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.*;
import java.security.PrivateKey;
import java.util.concurrent.TimeUnit;


public class Main extends Application {
    private DroneArena myArena;
    private ConsoleCanvas myCanvas;

    @Override
    public void start(Stage primaryStage) throws Exception{

        File dir = new File(".\\saves");
        File default_save = new File(".\\saves\\default.ser");

        //Create arena saves directory on first time run
        if(!dir.exists()){
            dir.mkdir();
        }

        //If saves directory is empty setup default arena config, otherwise load default arena save file
        if(dir.length() == 0){
            this.myArena = new DroneArena(20, 10);
            this.myArena.addDrone(1);
            this.myArena.addDrone(1);
            this.myArena.addDrone(2);
            this.myArena.addObstacle(1);
            this.myArena.addObstacle(2);
        }
        else if (default_save.exists()){
            try {
                FileInputStream fInStream = new FileInputStream(default_save);
                ObjectInputStream oInStream = new ObjectInputStream(fInStream);
                this.myArena = (DroneArena) oInStream.readObject();
            }
            catch (IOException | ClassNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
        //Fetch display dimensions to centre application window within screen.
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        int width = gd.getDisplayMode().getWidth();
        int height = gd.getDisplayMode().getHeight();

        Parent root = FXMLLoader.load(getClass().getResource("drone_gui.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, width/2.0, height/2.0));



        primaryStage.show();
    }

    void addArenaContentsObjects(){
        /**try {
            System.out.println("Enter an integer to select object type to add:");
            myArena.addObject(s.nextInt()); //Add drone to random location within arena
        }
        catch(Exception e) {
            System.err.println("Invalid input, please enter an integer.");
        }**/
    }

    void saveArena(){
        int response;
        File saveFile;

        //Creates file chooser at the package saves dir with file filters
        JFileChooser fileChooser = new JFileChooser(".\\saves");
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".ser", "ser");
        fileChooser.setFileFilter(filter);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        //Opens and scan contents of UI to save to disk
        response = fileChooser.showSaveDialog(null);
        FileOutputStream fOutStream;
        ObjectOutputStream oOutStream;
        if(response == JFileChooser.APPROVE_OPTION){
            saveFile = fileChooser.getSelectedFile();
            try {
                fOutStream = new FileOutputStream(saveFile);
                oOutStream = new ObjectOutputStream(fOutStream);
                oOutStream.writeObject(this.myArena);
            }
            catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }

        }
    }

    void loadArena(){
        int response;
        File loadFile;

        //Creates file chooser at the package saves dir with file filters
        JFileChooser fileChooser = new JFileChooser(".\\saves");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".ser", "ser");
        fileChooser.setFileFilter(filter);

        //Opens and scan contents of UI to load from disk
        response = fileChooser.showOpenDialog(null);
        if(response == JFileChooser.APPROVE_OPTION){
            loadFile = fileChooser.getSelectedFile();
            if(loadFile.isFile()){
                try {
                    FileInputStream fInStream = new FileInputStream(loadFile);
                    ObjectInputStream oInStream = new ObjectInputStream(fInStream);
                    this.myArena = (DroneArena) oInStream.readObject();
                }
                catch (IOException | ClassNotFoundException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
            }
        }
    }

    //PROVIDE GETTERS FOR ARENA ARRAYLISTS
    void resizeArena(){
        /**try {
            System.out.println("Entering new arena dimensions:\n Please enter X integer value.\n");
            int x = s.nextInt();
            System.out.println("Please enter Y integer value.\n");
            int y = s.nextInt();
            this.myArena.setSize(x, y);

            for (BasicDrone drone : this.myArena.drones){
                int adjust_drone_x = x, adjust_drone_y = y;
                while(drone.getX() > this.myArena.getLength()){
                    adjust_drone_x--;
                    drone.setX(adjust_drone_x);
                }
                while(drone.getY() > this.myArena.getHeight()){
                    adjust_drone_y--;
                    drone.setY(adjust_drone_y);
                }
            }
            for (ArenaObstacle obstacle: this.myArena.obstacles){
                int adjust_obstacle_x = x, adjust_obstacle_y = y;
                while(obstacle.getX() > this.myArena.getLength()){
                    adjust_obstacle_x--;
                    obstacle.setX(adjust_obstacle_x);
                }
                while(obstacle.getY() > this.myArena.getHeight()){
                    adjust_obstacle_y--;
                    obstacle.setY(adjust_obstacle_y);
                }
            }
        }
        catch (Exception e){
            System.err.println("Invalid input, please enter an integer.\n");
        }**/
    }

    void createNewArena(){
        /**try {
            System.out.println("Entering new arena dimensions:\n Please enter X integer value.\n");
            int x = s.nextInt();
            System.out.println("Please enter Y integer value.\n");
            int y = s.nextInt();
            this.myArena = new DroneArena(x, y);
        }
        catch (Exception e) {
            System.err.println("Invalid input, please enter an integer.\n");
        }**/
    }

    void moveArenaDrones(){
        /**
        System.out.println("Enter an integer for the distance to move:");
        int distance = 0;
        try {
            distance = s.nextInt();
        }
        catch(Exception e) {
            System.err.println("Invalid input, please enter an integer.");
        }

        if(!myArena.drones.isEmpty() && distance != 0){
            int i = 1;
            while(i <= distance){
                myArena.moveAllDrones(myArena); //Update drone locations
                this.myArena.checkStunTiles();
                doDisplay(); //Refresh display canvas
                try{
                    //delay switch between canvas refreshes to create animation
                    TimeUnit.MILLISECONDS.sleep(250);
                }
                catch (InterruptedException exception){
                    System.err.format("Exception: %s%n", exception);
                }
                i++;
            }
        }
        else {
            System.err.println("Arena is empty!");
        }**/
    }

    void DisplayArenaInfo(){
        System.out.print(myArena.toString());
    }

    void doDisplay(){
        int x = myArena.getLength();
        int y = myArena.getHeight();
        this.myCanvas = new ConsoleCanvas(x, y, this.myArena);
        this.myArena.showDrones(this.myCanvas);
        this.myArena.showObstacles(this.myCanvas);

        System.out.print(this.myCanvas.toString());
    }

    @FXML
    private void onClickEventPlay(MouseEvent mouseEvent) {
        System.out.println("mouseclick\n");
    }

    public void main(String[] args) {
        launch(args);
    }
}
